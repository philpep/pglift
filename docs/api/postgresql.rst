PostgreSQL
==========

.. currentmodule:: pglift.postgresql

Module :mod:`pglift.postgresql` exposes the following API to manipulate
a PostgreSQL cluster:

.. autofunction:: status
.. autofunction:: check_status
.. autofunction:: is_ready
.. autofunction:: replication_lag
