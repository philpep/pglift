.. currentmodule:: pglift.ctx

Execution context
=================

.. autoclass:: Context
    :members:
