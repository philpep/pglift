### Summary

<!-- Summarize the bug encountered concisely. -->

### Steps to reproduce

<!-- Describe how one can reproduce the issue. -->

### Site settings

<!--
   - Copy the output of 'pglift site-settings' command here.
   -
   - Also include files from `/etc/pglift` or `$XDG_CONFIG_HOME/pglift`
   - directories; typically `postgresql.conf`, `pg_hba.conf`, etc.
   -->
